# Welcome

This is EasyUnits - a slim Java 8 library for unit conversion with SI units, imperial units and whatever.

Current version: 0.0.1

# Packaging

* Build jars for compiled classes, javadoc and sources: `mvn package`
* Build jar-with-dependencies: `mvn assembly:single`

# Licence

This library is licenced under GPLv3 or any later version. See [LICENCE.md](LICENCE.md)

# Copyright

Copyright (C) 2018-2021 Timm Fitschen (mail@timmfitschen.de)

