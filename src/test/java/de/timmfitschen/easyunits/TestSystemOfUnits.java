/*
 * This is a file from EasyUnits - A slim library for units
 * Copyright (C) 2018 Timm Fitschen (mail@timmfitschen.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for moredetails.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package de.timmfitschen.easyunits;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;

import org.junit.Test;

import de.timmfitschen.easyunits.conversion.Converter;
import de.timmfitschen.easyunits.parser.DefaultCompoundUnitParser;
import de.timmfitschen.easyunits.parser.ParserException;

public class TestSystemOfUnits {
	
	SystemOfUnits empty = new SystemOfUnits(new DefaultSystemOfUnitsFactory().getAllunits());

	@Test(expected = NullPointerException.class)
	public void testInstantiationNull() {
		new SystemOfUnits(null);
	}

	@Test
	public void testGetUnitOneIsAllwaysThere() throws UnknownUnitException, ParserException {
		assertEquals(Unit.ONE, empty.getUnit("1"));
	}
	
	@Test
	public void testGetUnitCompound() throws UnknownUnitException, ParserException{
		BaseUnit m = new BaseUnit("m");
		HashMap<String, Unit> map = new HashMap<>();
		map.put("m", m);
		SystemOfUnits sou = new SystemOfUnits(map);
		assertEquals(m.times(m), sou.getUnit("m*m"));
	}
	
	@Test(expected=NullPointerException.class)
	public void testGetUnitNull() throws UnknownUnitException, ParserException{
		empty.getUnit(null);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testGetUnitEmptyString() throws UnknownUnitException, ParserException{
		empty.getUnit("");
	}
	
	@Test(expected=UnknownUnitException.class)
	public void testGetUnitNoneExisting() throws UnknownUnitException, ParserException{
		empty.getUnit("m");
	}
	
	@Test(expected=NullPointerException.class)
	public void setCompoundUnitParserNull(){
		empty.setCompoundUnitParser(null);
	}
	
	@Test
	public void setCompoundUnitParse(){
		DefaultCompoundUnitParser cup = new DefaultCompoundUnitParser();
		empty.setCompoundUnitParser(cup);
		assertSame(cup, empty.getCompoundUnitParser());
	}
	
	@Test
	public void testIsCommensurable(){
		assertFalse(empty.isCommensurable(new BaseUnit("m"), new BaseUnit("K")));
	}
	
	@Test(expected=UnknownUnitException.class)
	public void testIsCommensurableUnknownUnit() throws UnknownUnitException, ParserException{
		empty.isCommensurable("1", "m");
	}
	
	@Test
	public void testIsCommensurableOne() throws UnknownUnitException, ParserException{
		assertTrue(empty.isCommensurable("1", "1"));
	}
	
	@Test(expected=UnitIncommensurabilityException.class)
	public void testGetConverterIncommensurable() throws UnitIncommensurabilityException{
		empty.getConverter(new BaseUnit("m"), new BaseUnit("K"));
	}
	
	@Test
	public void testGetConverter() throws UnitIncommensurabilityException{
		assertEquals(Converter.IDENTITY, empty.getConverter(new BaseUnit("m"), new BaseUnit("m")));
	}
	
	@Test(expected=UnitIncommensurabilityException.class)
	public void testConvertIncommensurabl() throws UnitIncommensurabilityException{
		empty.convert(2.3, new BaseUnit("m"), new BaseUnit("n"));
	}
	
	@Test(expected=NullPointerException.class)
	public void testGetConverterNull() throws UnitIncommensurabilityException{
		empty.getConverter(null, new BaseUnit("m"));
	}
	
	@Test
	public void testConvert() throws UnitIncommensurabilityException{
		assertEquals(3.14, empty.convert(3.14, new BaseUnit("m"), new BaseUnit("m")).doubleValue(),0.0001);
	}

	@Test
	public void testConvertString()throws UnitIncommensurabilityException, UnknownUnitException, ParserException{
		assertEquals(3.14, empty.convert(3.14, "1", "1").doubleValue(),0.0001);
	}
	
	@Test
	public void testEquals(){
		HashMap<String, Unit> map1 = new HashMap<>();
		HashMap<String, Unit> map2 = new HashMap<>();
		
		assertEquals(map1.hashCode(),map2.hashCode());
		assertEquals(map1,map2);
		assertEquals(new SystemOfUnits(map1),new SystemOfUnits(map2));
		
		map2.put("m", new BaseUnit("m"));
		assertNotEquals(map1.hashCode(), map2.hashCode());
		assertNotEquals(map1,map2);
		assertNotEquals(new SystemOfUnits(map1),new SystemOfUnits(map2));
		
		map1.put("m", new BaseUnit("m"));
		assertEquals(map1.hashCode(),map2.hashCode());
		assertEquals(map1,map2);
		assertEquals(new SystemOfUnits(map1),new SystemOfUnits(map2));
	}
	
	@Test
	public void testNotEquals(){
		assertFalse(empty.equals("string"));
	}
	
	@Test public void testHash(){
		HashMap<String, Unit> map1 = new HashMap<>();
		
		assertEquals(new SystemOfUnits(map1).hashCode(), map1.hashCode());
	}
}
