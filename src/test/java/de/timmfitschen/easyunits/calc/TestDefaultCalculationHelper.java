/*
 * This is a file from EasyUnits - A slim library for units
 * Copyright (C) 2018 Timm Fitschen (mail@timmfitschen.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for moredetails.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package de.timmfitschen.easyunits.calc;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestDefaultCalculationHelper {

	public CalculationHelper helper = new DefaultCalculationHelper();

	@Test
	public void testSumSingleInteger() {
		assertEquals(0., helper.sum(0));
		assertEquals(1., helper.sum(1));
		assertEquals(new Double(Integer.MAX_VALUE), helper.sum(Integer.MAX_VALUE));
	}


	
	@Test
	public void testSum1PlusInteger() {
		assertEquals(1.0, helper.sum(1, 0));
		assertEquals(2.0, helper.sum(1, 1));
		assertEquals(1234.0, helper.sum(1, 1233));
	}
	
	@Test
	public void testSumNegZero() {
		assertEquals(0.0, helper.sum(-0));
		assertEquals(1.0, helper.sum(1, -0));
	}
	
	@Test
	public void testSumLong() {
		assertEquals(0.0, helper.sum(-0L));
		assertEquals(1.0, helper.sum(1L));
		assertEquals(123.0, helper.sum(100L, 23));
	}
	
	@Test
	public void testDouble(){
		assertEquals(0.0, (Double) helper.sum(0.0), 0.0001);
		assertEquals(6.1, (Double) helper.sum(0.0, 0.1, 1.0, 2, 3L), 0.0001);
	}
	

}
