/*
 * This is a file from EasyUnits - A slim library for units
 * Copyright (C) 2018 Timm Fitschen (mail@timmfitschen.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for moredetails.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package de.timmfitschen.easyunits;

import static org.junit.Assert.*;

import org.junit.Test;

import de.timmfitschen.easyunits.conversion.Prefix;

public class TestPrefixedUnit {
	
	PrefixedUnit kg = new PrefixedUnit(new BaseUnit("g"), new Prefix("k", 10, 3));
	
	@Test
	public void testInstantiation(){
		assertNotNull(kg);
	}
	
	@Test
	public void testIsPrefixable(){
		assertEquals("none", kg.getPrefixableBy());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testNonPrefixable(){
		new PrefixedUnit(new BaseUnit("K").setPrefixableBy("none"), new Prefix("k", 10, 3));
	}
	
	@Test(expected=NullPointerException.class)
	public void testNullUnit(){
		new PrefixedUnit(null, new Prefix("k", 10, 3));
	}

	@Test(expected=NullPointerException.class)
	public void testNullPrefix(){
		new PrefixedUnit(new BaseUnit("K"), null);
	}
}
