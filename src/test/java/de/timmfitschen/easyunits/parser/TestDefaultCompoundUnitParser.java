/*
 * This is a file from EasyUnits - A slim library for units
 * Copyright (C) 2018 Timm Fitschen (mail@timmfitschen.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for moredetails.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package de.timmfitschen.easyunits.parser;

import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.Test;

import de.timmfitschen.easyunits.BaseUnit;
import de.timmfitschen.easyunits.Unit;
import de.timmfitschen.easyunits.UnknownUnitException;

class TestCUP extends DefaultCompoundUnitParser {
}

public class TestDefaultCompoundUnitParser {

	TestCUP cup = new TestCUP();

	@Test
	public void testInstantiation() {
		assertNotNull(cup);
	}

	@Test
	public void testGetNextUnitReadFull() {
		assertEquals("asdf", cup.getNextUnit("asdf".toCharArray(), 0));
	}

	@Test
	public void testGetNextUnitReadUntilSlash() {
		assertEquals("asdf", cup.getNextUnit("asdf/sdfg".toCharArray(), 0));
	}

	@Test
	public void testGetNextUnitReadUntilStar() {
		assertEquals("asdf", cup.getNextUnit("asdf*sdfg".toCharArray(), 0));
	}

	@Test
	public void testGetNextUnitReadUntilUp() {
		assertEquals("asdf", cup.getNextUnit("asdf^sdfg".toCharArray(), 0));
	}

	@Test
	public void testGetNextUnitReadUntilLeft() {
		assertEquals("asdf", cup.getNextUnit("asdf(sdfg".toCharArray(), 0));
	}

	@Test
	public void testGetNextUnitReadUntilRight() {
		assertEquals("asdf", cup.getNextUnit("asdf)sdfg".toCharArray(), 0));
	}

	@Test
	public void testGetNextUnitReadFullFrom1() {
		assertEquals("asdf", cup.getNextUnit("qasdf".toCharArray(), 1));
	}

	@Test
	public void testGetNextUnitReadUntilSlashFrom1() {
		assertEquals("asdf", cup.getNextUnit("qasdf/sdfg".toCharArray(), 1));
	}

	@Test
	public void testGetNextUnitReadUntilStarFrom1() {
		assertEquals("asdf", cup.getNextUnit("qasdf*sdfg".toCharArray(), 1));
	}

	@Test
	public void testGetNextUnitReadUntilUpFrom1() {
		assertEquals("asdf", cup.getNextUnit("qasdf^sdfg".toCharArray(), 1));
	}

	@Test
	public void testGetNextUnitReadUntilLeftFrom1() {
		assertEquals("asdf", cup.getNextUnit("qasdf(sdfg".toCharArray(), 1));
	}

	@Test
	public void testGetNextUnitReadUntilRightFrom1() {
		assertEquals("asdf", cup.getNextUnit("qasdf)sdfg".toCharArray(), 1));
	}

	@Test
	public void testGetNextExponentReadFull() throws ParserException {
		assertEquals("1234", cup.getNextExponent("1234".toCharArray(), 0));
	}

	@Test
	public void testGetNextExponentReadFullNegative() throws ParserException {
		assertEquals("-1234", cup.getNextExponent("-1234".toCharArray(), 0));
	}

	@Test
	public void testGetNextExponentReadUntilLetter() throws ParserException {
		assertEquals("1234", cup.getNextExponent("1234a".toCharArray(), 0));
	}

	@Test
	public void testGetNextExponentReadUntilUp() throws ParserException {
		assertEquals("1234", cup.getNextExponent("1234^".toCharArray(), 0));
	}

	@Test
	public void testGetNextExponentReadFrom1() throws ParserException {
		assertEquals("1234", cup.getNextExponent("a1234".toCharArray(), 1));
	}

	@Test
	public void testGetNextExponentReadFullNegativeFrom1() throws ParserException {
		assertEquals("-1234", cup.getNextExponent("--1234".toCharArray(), 1));
	}

	@Test
	public void testGetNextExponentReadUntilLetterFrom1() throws ParserException {
		assertEquals("1234", cup.getNextExponent("%1234a".toCharArray(), 1));
	}

	@Test
	public void testGetNextExponentReadUntilUpFrom1() throws ParserException {
		assertEquals("1234", cup.getNextExponent("?1234^".toCharArray(), 1));
	}

	@Test
	public void testParseExponentNone() throws ParserException {
		int[] a = { 0, 0 };
		assertEquals(1, cup.parseExponent("asdf".toCharArray(), a));
		assertEquals(0, a[0]);
		assertEquals(1, cup.parseExponent("1234".toCharArray(), a));
		assertEquals(0, a[0]);
		assertEquals(1, cup.parseExponent("1^234".toCharArray(), a));
		assertEquals(0, a[0]);
	}

	@Test(expected = ParserException.class)
	public void testParseExponentEndsWithUp() throws ParserException {
		int[] a = { 0, 0 };
		cup.parseExponent("^".toCharArray(), a);
	}
	
	@Test(expected = ParserException.class)
	public void testParseExponentMinus() throws ParserException {
		int[] a = { 0, 0 };
		cup.parseExponent("^-".toCharArray(), a);
	}
	
	@Test(expected = ParserException.class)
	public void testParseExponentLetter() throws ParserException {
		int[] a = { 0, 0 };
		cup.parseExponent("^m".toCharArray(), a);
	}

	@Test
	public void testParseExponentIndexOutOfBound() throws ParserException {
		int[] a = { 2, 0 };
		assertEquals(1, cup.parseExponent("a".toCharArray(), a));
	}

	@Test
	public void testParseExponentFrom1() throws ParserException {
		int[] a = { 1, 0 };
		assertEquals(1234, cup.parseExponent("a^1234".toCharArray(), a));
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testParseSubExpressionIndexOutOfBounds() throws UnknownUnitException, ParserException {
		int[] a = { 0, 0 };
		cup.parseSubExpression(null, "".toCharArray(), a);
	}

	@Test(expected = NullPointerException.class)
	public void testParseSubExpressionAllUnitsNull() throws UnknownUnitException, ParserException {
		int[] a = { 0, 0 };
		cup.parseSubExpression(null, "asdf".toCharArray(), a);
	}

	@Test(expected = NullPointerException.class)
	public void testParseSubExpressionAllUnitsNullParenthesis() throws UnknownUnitException, ParserException {
		int[] a = { 0, 0 };
		cup.parseSubExpression(null, "(asdf".toCharArray(), a);
	}

	@Test(expected = UnknownUnitException.class)
	public void testParseSubExpressionUnknownUnit() throws UnknownUnitException, ParserException {
		int[] a = { 0, 0 };
		HashMap<String, Unit> map = new HashMap<>();
		cup.parseSubExpression(map, "asdf".toCharArray(), a);
	}

	@Test
	public void testParseSubExpressionNullUnit() throws UnknownUnitException, ParserException {
		int[] a = { 0, 0 };
		HashMap<String, Unit> map = new HashMap<>();
		map.put("asdf", null);
		assertNull(cup.parseSubExpression(map, "asdf".toCharArray(), a));
		assertEquals(4, a[0]);
	}

	@Test
	public void testParseSubExpressionBaseUnit() throws UnknownUnitException, ParserException {
		int[] a = { 0, 0 };
		Unit u = new BaseUnit("asdf");
		HashMap<String, Unit> map = new HashMap<>();
		map.put("asdf", u);
		assertSame(u, cup.parseSubExpression(map, "asdf".toCharArray(), a));
		assertEquals(4, a[0]);
	}

	@Test
	public void testParseSubExpressionNoBrackets() throws UnknownUnitException, ParserException {
		int[] a = { 0, 0 };
		Unit u = new BaseUnit("asdf");
		HashMap<String, Unit> map = new HashMap<>();
		map.put("asdf", u);
		assertEquals(u, cup.parseSubExpression(map, "asdf".toCharArray(), a));
		assertEquals(a[0], 4);
		assertEquals(a[1], 0);
	}

	@Test
	public void testParseCompound() throws UnknownUnitException, ParserException {
		int[] a = { 0, 0 };
		HashMap<String, Unit> map = new HashMap<>();
		BaseUnit m = new BaseUnit("m");
		map.put("m", m);
		assertEquals(m, cup.parseCompound(map, "m".toCharArray(), a));
	}

	@Test
	public void testParseCompoundMissinLeft() throws UnknownUnitException, ParserException {
		int[] a = { 0, 0 };
		HashMap<String, Unit> map = new HashMap<>();
		BaseUnit m = new BaseUnit("m");
		map.put("m", m);
		assertEquals(m, cup.parseCompound(map, "m)".toCharArray(), a));
		assertEquals(-1, a[1]);
	}

	@Test
	public void testParseCompoundMissinRight() throws UnknownUnitException, ParserException {
		int[] a = { 0, 0 };
		HashMap<String, Unit> map = new HashMap<>();
		BaseUnit m = new BaseUnit("m");
		map.put("m", m);
		assertEquals(m, cup.parseCompound(map, "(m".toCharArray(), a));
		assertEquals(1, a[1]);
	}

	@Test
	public void testParseCompoundMatching() throws UnknownUnitException, ParserException {
		int[] a = { 0, 0 };
		HashMap<String, Unit> map = new HashMap<>();
		BaseUnit m = new BaseUnit("m");
		map.put("m", m);
		assertEquals(m, cup.parseCompound(map, "(m)".toCharArray(), a));
		assertEquals(0, a[1]);
	}

	@Test
	public void testParseCompoundTimes() throws UnknownUnitException, ParserException {
		int[] a = { 0, 0 };
		HashMap<String, Unit> map = new HashMap<>();
		BaseUnit m = new BaseUnit("m");
		map.put("m", m);
		assertEquals(m.times(m), cup.parseCompound(map, "m*m".toCharArray(), a));
		assertEquals(0, a[1]);
	}

	@Test
	public void testParseCompoundDivide() throws UnknownUnitException, ParserException {
		int[] a = { 0, 0 };
		HashMap<String, Unit> map = new HashMap<>();
		BaseUnit m = new BaseUnit("m");
		map.put("m", m);
		assertEquals(m.divide(m), cup.parseCompound(map, "m/m".toCharArray(), a));
		assertEquals(0, a[1]);
	}

	
	@Test
	public void testParseCompoundString() throws UnknownUnitException, ParserException{
		HashMap<String, Unit> map = new HashMap<>();
		BaseUnit m = new BaseUnit("m");
		map.put("m", m);
		assertEquals(m.divide(m), cup.parseCompound(map, "m/m"));
	}
	
	@Test
	public void testParseCompoundMatchingBrackets1() throws UnknownUnitException, ParserException{
		HashMap<String, Unit> map = new HashMap<>();
		BaseUnit m = new BaseUnit("m");
		map.put("m", m);
		assertEquals(m.divide(m), cup.parseCompound(map, "(m/m)"));
	}
	
	@Test
	public void testParseCompoundMatchingBrackets2() throws UnknownUnitException, ParserException{
		HashMap<String, Unit> map = new HashMap<>();
		BaseUnit m = new BaseUnit("m");
		map.put("m", m);
		assertEquals(m.divide(m), cup.parseCompound(map, "(m/(m))"));
	}
	
	@Test
	public void testParseCompoundMatchingBrackets3() throws UnknownUnitException, ParserException{
		HashMap<String, Unit> map = new HashMap<>();
		BaseUnit m = new BaseUnit("m");
		map.put("m", m);
		assertEquals(m.divide(m), cup.parseCompound(map, "((m)/(m))"));
	}
	
	@Test
	public void testParseCompoundMatchingBrackets4() throws UnknownUnitException, ParserException{
		HashMap<String, Unit> map = new HashMap<>();
		BaseUnit m = new BaseUnit("m");
		map.put("m", m);
		assertEquals(m.divide(m), cup.parseCompound(map, "((m/m))"));
	}
	
	@Test(expected = ParserException.class)
	public void testParseCompoundMissingLeftBrackets1() throws UnknownUnitException, ParserException{
		HashMap<String, Unit> map = new HashMap<>();
		BaseUnit m = new BaseUnit("m");
		map.put("m", m);
		cup.parseCompound(map, "(m/m))");
	}
	
	@Test(expected = ParserException.class)
	public void testParseCompoundMissingLeftBrackets2() throws UnknownUnitException, ParserException{
		HashMap<String, Unit> map = new HashMap<>();
		BaseUnit m = new BaseUnit("m");
		map.put("m", m);
		cup.parseCompound(map, "m/m))");
	}
	
	@Test(expected = ParserException.class)
	public void testParseCompoundMissingLeftBrackets3() throws UnknownUnitException, ParserException{
		HashMap<String, Unit> map = new HashMap<>();
		BaseUnit m = new BaseUnit("m");
		map.put("m", m);
		cup.parseCompound(map, "(m)/m)");
	}
	
	@Test(expected = ParserException.class)
	public void testParseCompoundMissingRightBrackets1() throws UnknownUnitException, ParserException {
		HashMap<String, Unit> map = new HashMap<>();
		BaseUnit m = new BaseUnit("m");
		map.put("m", m);
		cup.parseCompound(map, "(m/m");
	}

	@Test(expected = ParserException.class)
	public void testParseCompoundMissingRightBrackets2() throws UnknownUnitException, ParserException{
		HashMap<String, Unit> map = new HashMap<>();
		BaseUnit m = new BaseUnit("m");
		map.put("m", m);
		cup.parseCompound(map, "(m)/(m");
	}
	
	@Test(expected = ParserException.class)
	public void testParseCompoundMissingRightBrackets3() throws UnknownUnitException, ParserException{
		HashMap<String, Unit> map = new HashMap<>();
		BaseUnit m = new BaseUnit("m");
		map.put("m", m);
		cup.parseCompound(map, "m)/(m)");
	}
	
	@Test(expected=ParserException.class)
	public void testParseCompoundMessedUpBrackets1() throws UnknownUnitException, ParserException{
		HashMap<String, Unit> map = new HashMap<>();
		BaseUnit m = new BaseUnit("m");
		map.put("m", m);
		cup.parseCompound(map, "(m(/m))");
	}

}
