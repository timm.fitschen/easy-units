/*
 * This is a file from EasyUnits - A slim library for units
 * Copyright (C) 2018 Timm Fitschen (mail@timmfitschen.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for moredetails.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package de.timmfitschen.easyunits.conversion;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestLinearConverter {

	@Test
	public void testConvertOffset() {
		// Kelvin to Celcius
		LinearConverter kelvin2celsius = new LinearConverter(-273.15, 1, 0);
//		assertEquals(0, kelvin2celsius.convert(273.15).doubleValue(), 0.000001);
		assertEquals(30, kelvin2celsius.convert(303.15).doubleValue(), 0.000001);
		
		// inverse
		assertEquals(273.15, kelvin2celsius.getInversion().convert(0).doubleValue(), 0.000001);
		assertEquals(276.15, kelvin2celsius.getInversion().convert(3).doubleValue(), 0.000001);
	}

	@Test
	public void testConvertSpread() {
		// m to ft
		LinearConverter m2ft = new LinearConverter(0, 32808.0/10000, 0);
		assertEquals(0, m2ft.convert(0).doubleValue(), 0.000001);
		assertEquals(3.2808, m2ft.convert(1).doubleValue(), 0.000001);
		
		// inverse
		assertEquals(0, m2ft.getInversion().convert(0).doubleValue(), 0.000001);
		assertEquals(1.0, m2ft.getInversion().convert(3.2808).doubleValue(), 0.000001);
	}
	
	@Test
	public void testInversion() {
		assertEquals(new LinearConverter(1.0, 1./3, 10.0).getInversion(), new LinearConverter(-10.0, 3, -1.0));
		assertEquals(new LinearConverter(1.0, 1./3, 0).getInversion(), new LinearConverter(0, 3, -1.0));
		assertEquals(new LinearConverter(0, 1./3, 10.0).getInversion(), new LinearConverter(-10.0, 3, 0));
		assertEquals(new LinearConverter(1.0, 1, 0).getInversion(), new LinearConverter(0, 1, -1.0));
		assertEquals(new LinearConverter(0, 1, 10.0).getInversion(), new LinearConverter(-10.0, 1, 0));
		assertEquals(new LinearConverter(0, 1, 0).getInversion(), new LinearConverter(0, 1, 0));
	}
	
	@Test
	public void testIdentity() {
		assertEquals(Converter.IDENTITY, new LinearConverter(0, 1, 0));
		assertEquals(Converter.IDENTITY, new LinearConverter(0, 1, 0).getInversion());
	}
	
	@Test
	public void testConcatenation() {
		LinearConverter c = new LinearConverter(1, 2./3, 4);
		assertEquals(c, Converter.IDENTITY.concatenate(c));
		assertEquals(c, c.concatenate(Converter.IDENTITY));
		
		LinearConverter c2 = new LinearConverter(5, 6./7, -8);
		assertEquals(new LinearConverter(1.0, 4./7, -.2857142857142865), c.concatenate(c2));
		assertEquals(new LinearConverter(5.0, 4./7, -.6666666666666661), c2.concatenate(c));
	}
	
	@Test(expected=UnsupportedOperationException.class)
	public void testNonLinear(){
		LinearConverter c = new LinearConverter(1, 2./3, 4);
		c.concatenate(new AbstractConverter() {
			
			@Override
			public long getSignature() {
				return 1234;
			}
			
			@Override
			public Number convert(Number value) {
				return 0;
			}
			
			@Override
			protected Converter concatenateNonIdentityConverter(Converter nextConverter) {
				return null;
			}
		});
	}
	
}
