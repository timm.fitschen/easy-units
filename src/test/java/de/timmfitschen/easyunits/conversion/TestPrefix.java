/*
 * This is a file from EasyUnits - A slim library for units
 * Copyright (C) 2018 Timm Fitschen (mail@timmfitschen.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for moredetails.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package de.timmfitschen.easyunits.conversion;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestPrefix {

	@Test
	public void testMicro() {
		Prefix micro = new Prefix("µ", 10, -3);
		assertEquals(micro, new LinearConverter(0, 1000, 0));
	}

	@Test
	public void testKilo() {
		Prefix kilo = new Prefix("k", 10, 3);
		assertEquals(kilo, new LinearConverter(0, 1./1000, 0));
	}

	@Test
	public void testGetSymbol() {
		Prefix kilo = new Prefix("k", 10, 3);
		assertEquals("k", kilo.getSymbol());
	}

	@Test
	public void testConcatenate() {
		Prefix k = new Prefix("k", 10, 3);
		Converter c = k.concatenate(new ProductConverter(1000));
		assertEquals(Converter.IDENTITY, c);
	}
	
	@Test
	public void testConvert() {
		Prefix milli = new Prefix("m", 10, -3);
		assertEquals(1000, milli.convert(1).doubleValue(), 0.00001);
	}
	
	@Test
	public void testGetInversion(){
		Converter milli = new Prefix("k", 10, 3).getInversion();
		assertEquals(milli, new Prefix("m", 10, -3));
	}

}
