/*
 * This is a file from EasyUnits - A slim library for units
 * Copyright (C) 2018 Timm Fitschen (mail@timmfitschen.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for moredetails.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package de.timmfitschen.easyunits;

import static org.junit.Assert.*;

import org.junit.Test;

import de.timmfitschen.easyunits.conversion.Converter;

public class TestBaseUnit {

	
	@Test
	public void testGetSymbol(){
		assertEquals("m", new BaseUnit("m").getSymbol());
	}
	
	@Test
	public void testGetSignature(){
		assertNotEquals(new BaseUnit("m").getSignature(), new BaseUnit("K").getSignature());
		assertEquals(new BaseUnit("V").getSignature(), new BaseUnit("V").getSignature());
	}
	
	@Test
	public void testGetNormalized(){
		BaseUnit m = new BaseUnit("m");
		assertTrue(m==m.getNormalizedUnit());
		assertTrue(m==m.add(-123.0).getNormalizedUnit());
		assertFalse(new BaseUnit("m")==m.getNormalizedUnit());
		assertFalse(new BaseUnit("K")==m.getNormalizedUnit());
		assertEquals(new BaseUnit("m"), m.getNormalizedUnit());
	}
	
	@Test
	public void testGetConverter(){
		assertEquals(new BaseUnit("asdf").getConverter(), Converter.IDENTITY);
	}
}
