/*
 * This is a file from EasyUnits - A slim library for units
 * Copyright (C) 2018 Timm Fitschen (mail@timmfitschen.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for moredetails.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package de.timmfitschen.easyunits;

import java.util.HashMap;
import java.util.HashSet;

import de.timmfitschen.easyunits.conversion.Prefix;

public class DefaultSystemOfUnitsFactory implements SystemOfUnitsFactory {

	public DefaultSystemOfUnitsFactory() {
		this.allunits.put(Unit.ONE.getSymbol(), Unit.ONE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.timmfitschen.easyunits.SystemOfUnitsFactory#create()
	 */
	@Override
	public SystemOfUnits create() {
		return new SystemOfUnits(allunits);
	}

	private final HashSet<String> prefixClasses = new HashSet<>();
	private final HashMap<String, Prefix> prefixNames = new HashMap<>();
	private final HashMap<String, Prefix> prefixes = new HashMap<>();
	private final HashMap<String, Unit> allunits = new HashMap<String, Unit>();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.timmfitschen.easyunits.SystemOfUnitsFactory#add(de.timmfitschen.
	 * easyunits.conversion.Prefix)
	 */
	@Override
	public void add(final Prefix p) throws SystemOfUnitsException {
		if (p == null) {
			throw new NullPointerException("Prefix was null.");
		}
		if(prefixClasses.contains(p.getSymbol())){
			throw new SystemOfUnitsException("SystemOfUnits is inconsistent: The prefix symbol "+ p.getSymbol() +" is already defined as a prefix class.");
		}
		if(prefixNames.containsKey(p.getSymbol())){
			throw new SystemOfUnitsException("SystemOfUnits is inconsistent: The prefix symbol"+ p.getSymbol() +" is already defined as an alias name for "+ prefixNames.get(p.getSymbol()).toString() + ".");
		}
		if(prefixes.containsKey(p.getSymbol())){
			throw new SystemOfUnitsException("SystemOfUnits is inconsistent: The prefix "+ p.getSymbol() +" is already defined as "+ prefixes.get(p.getSymbol()).toString() + ".");
		}
		for (String name : p.getNames()) {
			if(prefixClasses.contains(name)){
				throw new SystemOfUnitsException("SystemOfUnits is inconsistent: The alias name " + name + " is already defined as a prefix class.");
			}
			if(prefixNames.containsKey(name)){
				throw new SystemOfUnitsException("SystemOfUnits is inconsistent: The alias name " + name + " is already defined for prefix " + prefixNames.get(name).toString() + ".");
			}
			if(prefixes.containsKey(name)){
				throw new SystemOfUnitsException("SystemOfUnits is inconsistent: The alias name " + name + " is already defined as symbol for " + prefixes.get(name).toString() + ".");
			}
			this.prefixNames.put(name, p);
		}
		this.prefixes.put(p.getSymbol(),p);
		if(prefixNames.containsKey(p.getPrefixClass())){
			throw new SystemOfUnitsException("SystemOfUnits is inconsistent: The prefix class " + p.getPrefixClass() + " is already defined as an alias name for prefix " + prefixNames.get(p.getPrefixClass()).toString() + ".");
		}
		if(prefixes.containsKey(p.getPrefixClass())){
			throw new SystemOfUnitsException("SystemOfUnits is inconsistent: The prefix class " + p.getPrefixClass() + " is already defined as symbol for " + prefixes.get(p.getPrefixClass()).toString() + ".");
		}
		this.prefixClasses.add(p.getPrefixClass());
		for (Unit unit : allunits.values()) {
			if (unit.isPrefixableBy(p)) {
				addPrefixedUnit(p, unit);
			}
		}
	}

	protected void addUnitWithAllNames(Unit unit) throws SystemOfUnitsException {
		if (this.allunits.containsKey(unit.getSymbol())) {
			throw new SystemOfUnitsException("SystemOfUnits is inconsistent: " + unit.getSymbol()
					+ " is already defined as " + this.allunits.get(unit.getSymbol()).toString() + ".");
		}
		this.allunits.put(unit.getSymbol(), unit);
		for (String name : unit.getNames()) {
			if (name.equals(unit.getSymbol())) {
				continue;
			}
			if (this.allunits.containsKey(name)) {
				throw new SystemOfUnitsException("SystemOfUnits is inconsistent: " + unit.getSymbol()
						+ " is already defined as " + this.allunits.get(name).toString() + ".");
			}
			this.allunits.put(name, unit);
		}
		if (!unit.getPrefixableBy().equals("none"))
			for (Prefix prefix : prefixes.values()) {
				if (unit.isPrefixableBy(prefix)) {
					addPrefixedUnit(prefix, unit);
				}
			}
	}

	protected void addPrefixedUnit(Prefix prefix, Unit unit) throws SystemOfUnitsException {
		if (prefix == null) {
			throw new NullPointerException("Prefix was null.");
		}
		if (!this.allunits.containsKey(unit.getSymbol())) {
			throw new SystemOfUnitsException("SystemOfUnits is inconsistent: " + unit.getSymbol() + " is not defined.");
		}
		final PrefixedUnit prefixedUnit = new PrefixedUnit(unit, prefix);
		if (this.allunits.containsKey(prefixedUnit.getSymbol())) {
			throw new SystemOfUnitsException("SystemOfUnits is inconsistent: " + prefixedUnit.getSymbol()
					+ " is already defined  as " + this.allunits.get(prefixedUnit.getSymbol()).toString() + ".");
		}

		// combine each unit and each prefix (full names)
		for (final String unitName : unit.getNames()) {
			for (final String prefixName : prefix.getNames()) {
				String key = prefixName + unitName;
				if (this.allunits.containsKey(key)) {
					throw new SystemOfUnitsException("SystemOfUnits is inconsistent: " + key + " is already defined as "
							+ this.allunits.get(key).toString() + ".");
				}
				this.allunits.put(key, prefixedUnit);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.timmfitschen.easyunits.SystemOfUnitsFactory#add(de.timmfitschen.
	 * easyunits.DerivedUnit)
	 */
	@Override
	public void add(final DerivedUnit d) throws SystemOfUnitsException {
		addUnitWithAllNames(d);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.timmfitschen.easyunits.SystemOfUnitsFactory#add(de.timmfitschen.
	 * easyunits.BaseUnit)
	 */
	@Override
	public void add(final BaseUnit b) throws SystemOfUnitsException {
		addUnitWithAllNames(b);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.timmfitschen.easyunits.SystemOfUnitsFactory#getUnit(java.lang.String)
	 */
	@Override
	public Unit getUnit(final String unit) {
		if (this.allunits.containsKey(unit)) {
			return this.allunits.get(unit);
		}
		return null;
	}

	protected HashMap<String, Unit> getAllunits() {
		return allunits;
	}

}
