/*
 * This is a file from EasyUnits - A slim library for units
 * Copyright (C) 2018 Timm Fitschen (mail@timmfitschen.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for moredetails.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package de.timmfitschen.easyunits;

import de.timmfitschen.easyunits.conversion.OffsetConverter;
import de.timmfitschen.easyunits.conversion.Prefix;
import de.timmfitschen.easyunits.conversion.Converter;
import de.timmfitschen.easyunits.conversion.ProductConverter;

public abstract class Unit extends Symboled {

	/**
	 * Returns true iff this unit is normal. I.e. this unit is a BaseUnit or a
	 * CompoundUnit which consists only of BaseUnits.
	 * 
	 * @return true iff this unit is normal.
	 */
	final public boolean isNormal() {
		return Converter.IDENTITY.equals(getConverter());
	}
	
	public final static BaseUnit ONE = (BaseUnit) new BaseUnit("1").setPrefixableBy("none");

	private String prefixableBy = "all";

	/**
	 * Calculate the base unit or a compound unit that consists of base units
	 * only, without any prefixes. This is the target unit of the converter.
	 * 
	 * @return base unit/compound of base units.
	 */
	public abstract Unit getNormalizedUnit();

	/**
	 * Return a converter for a conversion of this unit to the normalized unit.
	 * 
	 * @return
	 */
	public abstract Converter getConverter();

	/**
	 * Transform this unit into another by means of a converter. That is
	 * basically a way of defining a new unit. E.g.
	 * {@code Unit degree_celcius = kelvin.transform(new
	 * AddConverter(-273.15));}
	 * 
	 * The converter defines the function which converts a value of the new unit
	 * into this unit.
	 * 
	 * 
	 * @param c
	 * @return
	 */
	protected Unit transform(final Converter c) {
		if (c == null || c.equals(Converter.IDENTITY)) {
			return this;
		} else {
			final Converter con = c.concatenate(this.getConverter());
			return new ConvertedUnit(this, con);
		}
	}

	public Unit add(Number n) {
		return this.transform(new OffsetConverter(n).getInversion());
	}

	public CompoundUnit times(Unit u) {
		return new CompoundUnitFactory().addComponent(this, 1).addComponent(u, 1).createUnit();
	}

	public Unit power(int i) {
		if(i==1) return this;
		return new CompoundUnitFactory().addComponent(this, i).createUnit();
	}

	public CompoundUnit divide(Unit u) {
		return new CompoundUnitFactory().addComponent(this, 1).addComponent(u, -1).createUnit();
	}

	public Unit times(Number n) {
		return transform(new ProductConverter(n).getInversion());
	}

	public Unit divide(Number n) {
		return transform(new ProductConverter(n));
	}

	public Unit setPrefixableBy(String prefixes) {
		this.prefixableBy = prefixes;
		return this;
	}
	
	public String getPrefixableBy() {
		return prefixableBy;
	}

	@Override
	public int hashCode() {
		Long l = getSignature();
		return l.hashCode();
	}

	public boolean isPrefixableBy(Prefix p) {
		String prefixable = getPrefixableBy();
		switch(prefixable){
		case "none":
			return false;
		case "all":
			return true;
		}
		
		for (String clz : prefixable.split(",")) {
			if(clz.equals(p.getSymbol()) || clz.equals(p.getPrefixClass()) || p.getNames().contains(clz)){
				return true;
			}
		}
		return false;
	}

}
