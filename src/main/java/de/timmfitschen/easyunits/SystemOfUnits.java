/*
 * This is a file from EasyUnits - A slim library for units
 * Copyright (C) 2018 Timm Fitschen (mail@timmfitschen.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for moredetails.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package de.timmfitschen.easyunits;

import java.util.Collections;
import java.util.Map;

import de.timmfitschen.easyunits.conversion.Converter;
import de.timmfitschen.easyunits.parser.CompoundUnitParser;
import de.timmfitschen.easyunits.parser.DefaultCompoundUnitParser;
import de.timmfitschen.easyunits.parser.ParserException;

public class SystemOfUnits {
	
	protected final Map<String, Unit> allunits;
	private CompoundUnitParser cup = new DefaultCompoundUnitParser();
	
	public SystemOfUnits(Map<String, Unit> allunits) {
		this.allunits = Collections.unmodifiableMap(allunits);
	}

    public Unit getUnit(final String unit) throws UnknownUnitException, ParserException  {
    	if(unit.length()==0){
    		throw new IllegalArgumentException("Unit was empty string.");
    	}
        if (this.allunits.containsKey(unit)) {
            return this.allunits.get(unit);
        } else {
            return this.parseCompound(unit);
        }
    }

    protected Unit parseCompound(String unit) throws UnknownUnitException, ParserException {
    	return this.getCompoundUnitParser().parseCompound(allunits, unit);
	}

	public Converter getConverter(Unit from, Unit to) throws UnitIncommensurabilityException {
		if(!isCommensurable(from, to)) {
			throw new UnitIncommensurabilityException("These units are incommensurable: "+from.getSymbol() + " and " + to.getSymbol());
		}
		return from.getConverter().concatenate(((Invertible) to.getConverter()).getInversion());
	}

	public boolean isCommensurable(Unit unit1, Unit unit2) {
		return unit1.getNormalizedUnit().equals(unit2.getNormalizedUnit());
	}
	
	public Number convert(Number value, Unit from, Unit to) throws UnitIncommensurabilityException {
		return getConverter(from, to).convert(value);
	}
	
	public Number convert(Number value, String fromUnit, String toUnit) throws UnitIncommensurabilityException, UnknownUnitException, ParserException {
		return convert(value, getUnit(fromUnit), getUnit(toUnit));
	}

	public CompoundUnitParser getCompoundUnitParser() {
		return cup;
	}

	public void setCompoundUnitParser(CompoundUnitParser cup) {
		if(cup==null){
			throw new NullPointerException("cup was null.");
		}
		this.cup = cup;
	}

	public boolean isCommensurable(String unit1, String unit2) throws UnknownUnitException, ParserException {
		return isCommensurable(this.getUnit(unit1), this.getUnit(unit2));
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof SystemOfUnits){
			return ((SystemOfUnits) obj).allunits.equals(this.allunits);
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return this.allunits.hashCode();
	}
}
