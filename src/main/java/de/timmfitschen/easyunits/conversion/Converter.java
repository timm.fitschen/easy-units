/*
 * This is a file from EasyUnits - A slim library for units
 * Copyright (C) 2018 Timm Fitschen (mail@timmfitschen.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for moredetails.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package de.timmfitschen.easyunits.conversion;

import de.timmfitschen.easyunits.Signed;

public interface Converter extends Signed {

	public static LinearConverter IDENTITY = new LinearConverter(0, 1, 0) {
		protected Converter concatenateNonIdentityConverter(Converter converter) {
			return converter;  
		};
	};

	/**
	 * Return a converter which is equivalent to applying the `nextConverter` on the
	 * result of a conversion with this converter. If `nextConverter` is null or the
	 * identity conversion, this converter is returned without instantiating a new
	 * converter. Otherwise, a new converter is instantiated while this converter is
	 * not altered.
	 * 
	 * @param nextConverter
	 * @return a converter
	 */
	public Converter concatenate(Converter nextConverter);

	/**
	 * Return the result of an application of this converter on `value`.
	 * 
	 * @param value
	 * @return result
	 */
	public Number convert(Number value);

}
