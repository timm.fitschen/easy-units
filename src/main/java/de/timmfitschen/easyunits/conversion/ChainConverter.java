/*
 * This is a file from EasyUnits - A slim library for units
 * Copyright (C) 2018 Timm Fitschen (mail@timmfitschen.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for moredetails.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package de.timmfitschen.easyunits.conversion;

import java.util.ArrayList;

/**
 * This class is currently not used but it will be useful as soon as non-linear
 * converters are implemented.
 * 
 * @author tf
 * 
 */
public class ChainConverter extends AbstractConverter {

    private ArrayList<Converter> converters = new ArrayList<Converter>();

    @Override
    protected Converter concatenateNonIdentityConverter(Converter c) {
        ChainConverter ret = new ChainConverter();
        ret.converters.addAll(this.converters);
        if (c instanceof ChainConverter) {
            ret.converters.addAll(((ChainConverter) c).converters);
        } else {
            ret.converters.add(c);
        }
        return ret;
    }

    @Override
    public long getSignature() {
        long hash = 0;
        for (Converter c : converters) {
            hash += c.getSignature();
        }
        return hash;
    }


	@Override
	public Number convert(Number value) {
		throw new UnsupportedOperationException("Not implemented.");
	}

}
