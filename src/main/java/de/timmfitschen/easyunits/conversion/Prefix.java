/*
 * This is a file from EasyUnits - A slim library for units
 * Copyright (C) 2018 Timm Fitschen (mail@timmfitschen.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for moredetails.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package de.timmfitschen.easyunits.conversion;

import de.timmfitschen.easyunits.Invertible;
import de.timmfitschen.easyunits.Symboled;
import de.timmfitschen.easyunits.calc.CalculationHelper;



public class Prefix extends Symboled implements Invertible {
	private String symbol;
	private ProductConverter converter;
	private CalculationHelper helper = CalculationHelper.getCalculationHelper();
	private String prefixClass = null;
	public CalculationHelper getHelper() {
		return helper;
	}
	
	public void setHelper(CalculationHelper helper) {
		this.helper = helper;
	}

	public Prefix(String symbol, int base, int exponent) {
		this.converter = new ProductConverter(helper.pow(base, -exponent));
		this.symbol = symbol;
	}

	@Override
	public long getSignature() {
		return getConverter().getSignature();
	}

	@Override
	public Converter concatenate(Converter nextConverter) {
		return converter.concatenate(nextConverter);
	}

	@Override
	public Number convert(Number value) {
		return converter.convert(value);
	}

	@Override
	public Invertible getInversion() {
		return converter.getInversion();
	}
	
	public ProductConverter getConverter(){
		return converter;
	}

	@Override
	public String getSymbol() {
		return symbol;
	}

	public String getPrefixClass() {
		return prefixClass;
	}
	
	public void setPrefixClass(String prefixClass) {
		this.prefixClass = prefixClass;
	}

}
