/*
 * This is a file from EasyUnits - A slim library for units
 * Copyright (C) 2018 Timm Fitschen (mail@timmfitschen.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for moredetails.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package de.timmfitschen.easyunits;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import de.timmfitschen.easyunits.conversion.Converter;

public class CompoundUnit extends Unit {

	private Map<Unit, Integer> components;
	
	public CompoundUnit(Map<Unit, Integer> components) {
		if(components.isEmpty()){
			throw new IllegalArgumentException("These components have been empty.");
		}
		this.components = Collections.unmodifiableMap(new HashMap<>(components));
	}

	public Map<Unit, Integer> getComponents() {
		return components;
	}

	@Override
	public Unit getNormalizedUnit() {
		CompoundUnitFactory c = new CompoundUnitFactory();
		for (Entry<Unit, Integer> set : components.entrySet()) {
			c.addComponent(set.getKey().getNormalizedUnit(), set.getValue());
		}
		return c.createUnit();
	}

	@Override
	public Converter getConverter() {
		Converter ret = Converter.IDENTITY;
		for (Entry<Unit, Integer> set : this.components.entrySet()) {
			if (set.getValue() < 0) {
				for (int i = set.getValue(); i < 0; i++) {
					ret = ret.concatenate(((Invertible) set.getKey().getConverter()).getInversion());
				}
			} else {
				for (int i = 0; i < set.getValue(); i++) {
					ret = ret.concatenate(set.getKey().getConverter());
				}
			}
		}
		return ret;
	}

	@Override
	public String getSymbol() {
		if (components.size() == 1) {
			for (Entry<Unit, Integer> set : components.entrySet()) {
				int exp = set.getValue();
				if (exp == 1) {
					return set.getKey().getSymbol();
				}
			}
		}
		return null;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append('[');
		for (Entry<Unit, Integer> set : components.entrySet()) {
			sb.append('(');
			sb.append(set.getKey().getSymbol());
			sb.append(',');
			sb.append(set.getValue());
			sb.append(')');
		}
		sb.append(']');
		return sb.toString();
	}

	@Override
	public long getSignature() {
		long hash = 0;
		for (Entry<Unit, Integer> set : components.entrySet()) {
			hash += (set.getValue() != 0 ? set.getValue() : 0.5) * set.getKey().getSignature();
		}
		return hash;
	}

}
