/*
 * This is a file from EasyUnits - A slim library for units
 * Copyright (C) 2018 Timm Fitschen (mail@timmfitschen.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for moredetails.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package de.timmfitschen.easyunits;

import java.util.HashMap;
import java.util.Map.Entry;

public class CompoundUnitFactory {

	private HashMap<Unit, Integer> components = new HashMap<Unit, Integer>();

	public CompoundUnit createUnit() {
		return new CompoundUnit(components);
	}

	public CompoundUnitFactory addComponent(Unit u, int exponent) {
		if(u==null){
			throw new NullPointerException("Unit was null");
		}
		if (u instanceof CompoundUnit) {
			this.addComponent((CompoundUnit) u, exponent);
		} else if (components.containsKey(u)) {
			components.put(u, exponent + components.get(u));
		} else {
			components.put(u, exponent);
		}
		return this;
	}

	public CompoundUnitFactory addComponent(CompoundUnit u, int exponent) {
		if(u==null){
			throw new NullPointerException("Unit was null");
		}
		for (Entry<Unit, Integer> set : u.getComponents().entrySet()) {
			addComponent(set.getKey(), exponent * set.getValue());
		}
		return this;
	}
}
