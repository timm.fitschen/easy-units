/*
 * This is a file from EasyUnits - A slim library for units
 * Copyright (C) 2018 Timm Fitschen (mail@timmfitschen.de)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for moredetails.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package de.timmfitschen.easyunits.parser;

import java.util.Map;

import de.timmfitschen.easyunits.CompoundUnit;
import de.timmfitschen.easyunits.CompoundUnitFactory;
import de.timmfitschen.easyunits.Unit;
import de.timmfitschen.easyunits.UnknownUnitException;

public class DefaultCompoundUnitParser implements CompoundUnitParser {

	protected Unit parseSubExpression(Map<String, Unit> allunits, final char[] cs, final int[] status)
			throws UnknownUnitException, ParserException {
		Unit nextUnit;
		switch (cs[status[0]]) {
		case '(':
			status[0]++;
			status[1]++;
			nextUnit = this.parseCompound(allunits, cs, status);
			break;
		default:
			final String next = this.getNextUnit(cs, status[0]);
			if (allunits.containsKey(next)) {
				nextUnit = allunits.get(next);
				status[0] += next.length();
			} else {
				throw new UnknownUnitException("Unknown unit " + next);
			}
		}
		
		return nextUnit;
	}

	protected int parseExponent(final char[] cs, final int[] status) throws ParserException {
		int exp = 1;
		if (status[0] < cs.length) {
			switch (cs[status[0]]) {
			case '^':
				if (status[0] == cs.length - 1) {
					throw new ParserException("This compound unit ended with a '^'. Exponent expected.");
				}
				status[0]++;
				final String exp_str = this.getNextExponent(cs, status[0]);
				try{
					exp = Integer.valueOf(exp_str);
				} catch (NumberFormatException e){
					throw new ParserException("This is not a valid exponent: " + exp_str);
				}
				status[0] += exp_str.length();

				break;
			default:
				break;
			}
		}
		return exp;
	}

	@Override
	public CompoundUnit parseCompound(Map<String, Unit> allunits, final String unit)
			throws UnknownUnitException, ParserException {
		int[] status = { 0, 0 };// position of the parser, level of parenthesis
		CompoundUnit ret = parseCompound(allunits, unit.toCharArray(), status);
		if (status[1] > 0) {
			throw new ParserException("This compound unit is missing closing brackets ')'.");
		} else if (status[1] < 0){
			throw new ParserException("This compound unit is missing opening brackets '('.");
		}
		return ret;
	}

	protected CompoundUnit parseCompound(Map<String, Unit> allunits, final char[] cs, final int[] status)
			throws UnknownUnitException, ParserException {
		final CompoundUnitFactory ret = new CompoundUnitFactory();
		Unit nextUnit = this.parseSubExpression(allunits, cs, status);
		int exp = this.parseExponent(cs, status);

		ret.addComponent(nextUnit, exp);

		w: while (status[0] < cs.length) {
			switch (cs[status[0]]) {
			case '/':
				status[0]++;
				nextUnit = this.parseSubExpression(allunits, cs, status);
				exp = this.parseExponent(cs, status);
				ret.addComponent(nextUnit, -1 * exp);
				break;
			case '*':
				status[0]++;
				nextUnit = this.parseSubExpression(allunits, cs, status);
				exp = this.parseExponent(cs, status);
				ret.addComponent(nextUnit, exp);
				break;
			case ')':
				status[0]++;
				status[1]--;
				break w;
			default:
				// e.g. "(m(/m))"
				throw new ParserException("Unexpected char: " + cs[status[0]]);
			}
		}

		return ret.createUnit();
	}

	protected String getNextExponent(final char[] cs, int pos) throws ParserException {
		final StringBuilder sb = new StringBuilder();
		char c = cs[pos];
		if (c == '-') {
			sb.append(c);
			pos++;
		}
		
		while (pos < cs.length) {
			c = cs[pos];
			switch (c) {
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				sb.append(c);
				break;
			default:
				return sb.toString();
			}
			pos++;
		}
		return sb.toString();
	}

	protected String getNextUnit(final char[] cs, int pos) {
		final StringBuilder sb = new StringBuilder();
		while (pos < cs.length) {
			final char c = cs[pos];
			switch (c) {
			case '/':
			case '*':
			case '^':
			case '(':
			case ')':
				return sb.toString();
			default:
				sb.append(c);
				pos++;
				break;
			}
		}
		return sb.toString();
	}

}
